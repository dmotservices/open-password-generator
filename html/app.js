/**
 * core.js
 */

// NOTE: jquery code deliberately excluded from this file.

MyPasswordGenerator =
{
	eyeToggle : function()
	{
		let e = document.f;
		
		if (!e)
		{
			MyPasswordGenerator.status("Fatal error while processing the form.");
			return;
		}
		
		let fieldType = (e.master.type == 'text' ? 'password' : 'text');
		e.master.type=fieldType;
	},
	
	main : function()
	{
		let e = document.f;
		if (!e)
		{
			MyPasswordGenerator.status("Fatal error while processing the form.");
			return;
		}

		let password = "";
		let master = e.master.value.toUpperCase().trim();
		let site = e.site.value.trim();
		let user = (null == e.user.value) ? '' : e.user.value;
		let PASS_LENGTH = 8;

		if (user.length > 0)
			site = user + '@' + site;
		
		if (master == '')
		{
			MyPasswordGenerator.status("Master cannot be blank");
		} else if (site == '')
		{
			MyPasswordGenerator.status("Site cannot be blank");
		} else
		{
			password = MyPasswordGenerator.generatePassword(md5.hex_md5(master + ':' + site)).substring(0,
					PASS_LENGTH)
					+ MyPasswordGenerator.generatePassword(md5.hex_md5(site + ':' + master)).substring(0,
							PASS_LENGTH);
		}
		
		let p = document.getElementById('generated');
		p.value = password;
        p.select();

		if (password != "")
			MyPasswordGenerator.status("");
	},
	
	/*
	 * Converts a 32-char hex md5 string to an 8-char str radix 64 64**x ==
	 * 16**y => 2**(6x) == 2**(4y) => 6x == 4y so, 3 radix 16 chars can produce
	 * 2 radix 64 chars (16**3 = 4,096 and 64**2 = 4,096)
	 */
	generatePassword : function(md5_str)
	{
		let fieldSize = 3;
		let az_tab = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!";
		let str = "";

		// pre-condition: md5_str of length 32 chars
		if (md5_str.length == 32)
		{

			for (let i = md5_str.length; i > 0; i -= fieldSize)
			{
				let offset = (i - fieldSize) > 0 ? i - fieldSize : 0;
				let md5segment = md5_str.substring(offset, i);
				let fieldSum = parseInt(md5segment, 16);

				let smallEnd = fieldSum & 0x39; // retrieve lower 64 bits
				let bigEnd = fieldSum >> 6; // retrieve everything above lower 64 bits

				str += az_tab.charAt(smallEnd);
				if (i - offset >= fieldSize)
				{
					str += az_tab.charAt(bigEnd);
				}

			}

		} else
		{
			MyPasswordGenerator.status("32-char md5 hex string expected as input to generatePassword()");
		}

		return str; // return str of len 8 chars
	},
	
	reset: function()
	{
		let e = document.f;
		if (!e)
		{
			MyPasswordGenerator.status("Fatal error while processing the form.");
			return;
		}
		
		e.master.value = '';
		e.site.value = '';
		e.username.value = '';
		e.generated.value = '';
	},
	
	status : function(msg)
	{
		document.getElementById('status').innerHTML = msg;
	}
};